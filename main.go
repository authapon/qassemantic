package qassemantic

import (
	"errors"
	moosql "gitlab.com/authapon/moosqlite"
	"strings"
)

type (
	SemanticDB struct {
		Concepts  []SemanticConcept
		Relations []SemanticRelation
	}

	SemanticConcept struct {
		Semantic int
		Word     string
		POS      string
	}

	SemanticRelation struct {
		Agent    int
		Relation string
		Object   int
	}
)

func SaveConcepts(word string, pos string, semantic int) {
	db, err := moosql.GetSQL()
	if err != nil {
		return
	}
	defer db.Close()

	con, _ := db.Begin()
	defer con.Commit()

	st, _ := con.Prepare("insert into `semantic_dict` (`word`,`pos`, `semantic`) values (?,?,?);")
	_, err = st.Exec(word, pos, semantic)
}

func GetSemDB() (SemanticDB, error) {
	semDB := SemanticDB{}
	semDB.Concepts = make([]SemanticConcept, 0)
	semDB.Relations = make([]SemanticRelation, 0)

	db, err := moosql.GetSQL()
	if err != nil {
		return semDB, errors.New("DB connection fail!")
	}
	defer db.Close()
	con, _ := db.Begin()
	defer con.Commit()

	st, _ := con.Prepare("select `semantic`, `word`, `pos` from `semantic_dict`;")
	rows, err := st.Query()
	if err != nil {
		return semDB, errors.New("DB query fail!")
	}
	defer rows.Close()

	for rows.Next() {
		semantic := 0
		word := ""
		pos := ""
		rows.Scan(&semantic, &word, &pos)
		semConcept := SemanticConcept{}
		semConcept.Semantic = semantic
		semConcept.Word = word
		semConcept.POS = pos
		semDB.Concepts = append(semDB.Concepts, semConcept)
	}

	st, _ = con.Prepare("select `agent`, `relation`, `object` from `semantic_relation`;")
	rows, err = st.Query()
	if err != nil {
		return semDB, errors.New("DB query fail!")
	}
	defer rows.Close()

	for rows.Next() {
		agent := 0
		object := 0
		relation := ""
		rows.Scan(&agent, &relation, &object)
		semRelation := SemanticRelation{}
		semRelation.Agent = agent
		semRelation.Relation = relation
		semRelation.Object = object
		semDB.Relations = append(semDB.Relations, semRelation)
	}

	return semDB, nil
}

func (db SemanticDB) Exist(c int) bool {
	for i := range db.Concepts {
		if db.Concepts[i].Semantic == c {
			return true
		}
	}
	return false
}

func (db SemanticDB) ISA(c1, c2 int) bool {
	if !db.Exist(c1) {
		return false
	}
	if !db.Exist(c2) && c2 != 0 {
		return false
	}

	for i := range db.Relations {
		if db.Relations[i].Agent == c1 {
			if db.Relations[i].Relation == "isa" {
				if c2 == 0 {
					return false
				}
				if db.Relations[i].Object == c2 {
					return true
				}
				if db.ISA(db.Relations[i].Object, c2) {
					return true
				}
			}
		}
	}
	if c2 == 0 {
		return true
	}
	return false
}

func (db SemanticDB) ISAWord(s1, s2 string) bool {
	w1 := strings.Split(s1, ":")
	if len(w1) != 2 {
		return false
	}
	w2 := strings.Split(s2, ":")
	if len(w2) != 2 {
		return false
	}
	c1 := make([]int, 0)
	c2 := make([]int, 0)
	for i := range db.Concepts {
		if db.Concepts[i].Word == w1[0] && db.Concepts[i].POS == w1[1] {
			c1 = append(c1, db.Concepts[i].Semantic)
		}
	}
	for i := range db.Concepts {
		if db.Concepts[i].Word == w2[0] && db.Concepts[i].POS == w2[1] {
			c2 = append(c2, db.Concepts[i].Semantic)
		}
	}
	for i := range c1 {
		for ii := range c2 {
			if db.ISA(c1[i], c2[ii]) || db.ISA(c2[ii], c1[i]) {
				return true
			}
		}
	}
	return false
}

func (db SemanticDB) ISAall(c int) []int {
	result := make([]int, 0)
	result = append(result, c)
	for i := range db.Relations {
		if db.Relations[i].Agent == c && db.Relations[i].Relation == "isa" {
			result = append(result, db.Relations[i].Object)
			r := db.ISAall(db.Relations[i].Object)
			for ii := range r {
				check := true
				for iii := range result {
					if result[iii] == r[ii] {
						check = false
						break
					}
				}
				if check {
					result = append(result, r[ii])
				}

			}
		}
	}
	return result
}

func (db SemanticDB) IsPartOfSomething(c int) bool {
	for i := range db.Relations {
		if db.Relations[i].Object == c && db.Relations[i].Relation == "hasa" {
			return true
		}
	}
	return false
}

func (db SemanticDB) IsPartOfSomethingAll(c int) bool {
	cc := db.ISAall(c)
	for i := range cc {
		if db.IsPartOfSomething(cc[i]) {
			return true
		}
	}
	return false
}

func (db SemanticDB) IsPartOfSomethingAllFromWord(w, pos string) bool {
	sem := make([]int, 0)
	for i := range db.Concepts {
		if db.Concepts[i].Word == w && db.Concepts[i].POS == pos {
			sem = append(sem, db.Concepts[i].Semantic)
		}
	}
	for ii := range sem {
		if db.IsPartOfSomethingAll(sem[ii]) {
			return true
		}
	}
	return false
}
